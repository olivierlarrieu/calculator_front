import React from 'react'
import { Button, FormControl } from 'react-bootstrap'
import { Alert, Row, Divider, Counter, Header, Select } from './components'
import { getCounter, processCalcul, resetHistory } from './ApiClient'
import './main.css'


const HISTORY_URL = "http://localhost:8000/apis/history"


class Calculator extends React.Component {

    state = {
        serverError: false,
        counterValue: 0,
        selectedOperator: false,
        operators: [
            { name: 'Addition', value: '+', label: '+' },
            { name: 'Soustraction', value: '-', label: '-' },
            { name: 'Division', value: '/', label: '/' },
            { name: 'Multiplication', value: '*', label: 'x' }
        ],
        inputValue: undefined
    }

    componentWillMount() {
        this.getCounter()
    }

    setValue = (event) => {
        this.setState({ inputValue: event.target.value })
    }

    setOperator = (operator) => {
        this.setState({ selectedOperator: operator })
    }

    getCounter = () => {
        getCounter(
            (value) => { this.setState({ counterValue: value, serverError: false }) },
            (value) => { this.setState({ serverError: "server error" }) }
        )
    }

    resetHistory = () => {
        resetHistory(
            (value) => { this.setState({ counterValue: 0, serverError: false }) },
            (error) => { this.setState({ serverError: "server error" }) }
        )
    }

    processCalcul = () => {
        processCalcul(
            this.state.selectedOperator,
            this.state.inputValue,
            (value) => { this.setState({ counterValue: value.toFixed(1), serverError: false }) },
            (error) => { this.setState({ serverError: "server error" }) }
        )
    }

    render() {
        return (

            <div className="calculator-container">

                <Header historyUrl={HISTORY_URL} onClear={this.resetHistory} showClear={this.state.counterValue > 0 || this.state.counterValue < 0} />

                <Row>

                    <Counter value={this.state.counterValue} />

                </Row>
                {
                    this.state.serverError &&
                    <Alert />
                }
                {
                    !this.state.serverError &&
                    <Divider />
                }
                <Row>

                    <Select
                        value={this.state.selectedOperator}
                        operators={this.state.operators}
                        onSelect={this.setOperator}
                    />

                </Row>

                <Divider />

                <Row>

                    <FormControl
                        type="number"
                        placeholder="Valeur"
                        onChange={this.setValue} />

                </Row>

                <Divider />

                <Row style={{ textAlign: "center" }}>
                    <Button
                        bsStyle="primary"
                        disabled={this.state.selectedOperator && this.state.inputValue ? false : true}
                        onClick={this.processCalcul}>VALIDER</Button>
                </Row>

            </div>

        )

    }

}

export default Calculator
