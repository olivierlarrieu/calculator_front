import React from 'react'


const Counter = ({value}) => {

    // adjust font size
    let fontSize = 150
    if (parseInt(value) !== 0) {
        fontSize = fontSize - value.length * 10
    }

    return (

    <div className="calculator-counter" style={{fontSize: fontSize + 'px'}}>
        {value}
    </div>
    )

}

export default Counter
