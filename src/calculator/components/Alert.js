import React from 'react'
import { Alert } from 'react-bootstrap'


const CalculatorAlert = () => (

    <Alert bsStyle="danger" >
    <h4>Une erreur est survenue, veuillez contacter le support client.</h4>
    </Alert>

)

export default CalculatorAlert
