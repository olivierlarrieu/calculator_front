import React from 'react'


const Header = ({ historyUrl, showClear, onClear }) => (

    <div className="calculator-header" >
        {
            showClear &&
            <span className="calculator-header-textbtn" onClick={onClear}>
                CLEAR
            </span>
        }


        <a className="calculator-header-textbtn pull-right" href={historyUrl} target="_blank" rel="noopener noreferrer">
            ACCEDER A L'HISTORIQUE
        </a>

    </div>

)

export default Header
