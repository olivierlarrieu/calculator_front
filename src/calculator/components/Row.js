import React from 'react'
import {  Row, Col } from 'react-bootstrap'


const CalculatorRow = (props) => (

    <Row>
        <Col xs={3} sm={3} md={3} lg={3}></Col>
        <Col xs={6} sm={6} md={6} lg={6} style={props.style}>
            { props.children }
        </Col>
        <Col xs={3} sm={3} md={3} lg={3}></Col>
    </Row>

)

export default CalculatorRow
