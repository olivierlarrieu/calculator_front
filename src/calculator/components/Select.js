import React from 'react'
import { ListGroup, ListGroupItem } from 'react-bootstrap'


const Select = ({ operators, value, onSelect }) => {
    
    return (

        <ListGroup>
            {
                operators.map((op, idx) => (

                    <ListGroupItem
                        key={idx}
                        href={"#link" + idx}
                        onClick={()=>{ onSelect(op.value)} }
                        active={value === op.value ? true:false}
                    >
                        <div>{op.name} <span className="calculator-operator-badge" >{op.label}</span></div>
                    </ListGroupItem>

                ))
            }
        </ListGroup>

    )
}

export default Select
