
const RESET_HISTORY_URL = "http://localhost:8000/apis/history/?action=reset"
const COUNTER_URL = "http://localhost:8000/apis/counter/"


const getCounter = (cb, onError) => {
        let headers = new Headers()
        headers.set('Content-Type', 'application/json')
        headers.set('Accept', 'application/json')

        fetch(`${COUNTER_URL}`, { method: "GET" })
            .then((resp) => resp.json())
            .then((content) => {
                let value = content.result.toFixed(1)
                if (content.result === 0) {
                    value = 0
                }
                cb(value)
            })
            .catch(function (error) {
                onError(error)
            });
    }

const resetHistory = (cb, onError) => {
        let headers = new Headers()
        headers.set('Content-Type', 'application/json')
        headers.set('Accept', 'application/json')

        fetch(`${RESET_HISTORY_URL}`, { method: "GET" })
            .then((resp) => resp.json())
            .then((content) => {
                cb(0)
            })
            .catch(function (error) {
                onError(error)
            });
    }

const processCalcul = (operator, value, cb, onError) => {
        let headers = new Headers()
        headers.set('Content-Type', 'application/json')
        headers.set('Accept', 'application/json')
        let body = JSON.stringify({ operator: operator, value: value })

        fetch(`${COUNTER_URL}`, { method: "POST", body: body, headers: headers })
            .then((resp) => resp.json())
            .then((content) => {
                cb(content.result)
            })
            .catch(function (error) {
                onError(error)
            });
    }

export {
    getCounter,
    resetHistory,
    processCalcul
}